# Simple and Clean: Pelican Blog Theme

A basic responsive blog theme for the [Pelican](https://getpelican.com/) static site generator. Bundled with the [Bulma](https://bulma.io/) CSS framework and [FontAwesome 5](https://fontawesome.com/) for easy customization. Zero JavaScript - add it if you want.

## Installation

1. Install Pelican via pip and configure your site. The Pelican [quickstart guide](https://docs.getpelican.com/en/stable/quickstart.html) is a good resource.
2. Clone this repo: 
> `git clone https://gitlab.com/federalfarmer/simple-and-clean.git`
3. Specify this theme in your **pelicanconf.py** file: 
> `THEME = 'path/to/simpleandclean'`
4. Run: 
> `pelican --listen`

## Usage

- **Avatar**: Your avatar is referenced on Line 50 of `base.html` and assumes a 150 x 150 pixel image file. To change, place an image in the `static/images` directory and reference it in the appropriate template.

- **Pages**: Static pages are automatically added to your top navigation bar. The *Blog* link is hard-coded to your homepage on Line 69 of `base.html`. Feel free to change this behavior if it's not to your liking.

- **Categories**: Links to articles by category are automatically added to the *Categories* box in the right sidebar.

- **Social Links**: Social media links are hard-coded on Lines 57-62 of `base.html`. Please note that these links do *not* use the `SOCIAL` tuple in `pelicanconf.py` - they are strictly hard-coded in this version of the theme. A few example social media icons are used but you can easily add, remove, or change them with the bundled FontAwesome.
	- The RSS button links to `/feeds/all.atom.xml` by default but can be changed to any of Pelican's other [generated feeds](https://docs.getpelican.com/en/stable/settings.html?highlight=feeds#feed-settings).
	- When wrapping FontAwesome icons in `<a>` tags for use as links, add the CSS class `social-btn` to override default link styling.
